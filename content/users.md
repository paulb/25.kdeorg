---
layout: users
title: User support
menu:
  main:
    weight: 3
name: Elisa
forums: https://forum.kde.org/
handbook: https://docs.kde.org/stable5/en/elisa/elisa/index.html
---
