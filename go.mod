module elisa-kde-org

go 1.15

require (
	github.com/gohugoio/hugo-mod-bootstrap-scss-v4 v1.0.0 // indirect
	github.com/thednp/bootstrap.native v0.0.0-20210904065248-eb3bad3a8c96 // indirect
	github.com/twbs/bootstrap v5.1.1+incompatible // indirect
	invent.kde.org/websites/aether-sass v0.0.0-20210419094803-abd3f54275fc // indirect
)
